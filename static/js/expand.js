	function clickme(expand) {
	expand = $(expand)
	var  expandcontent = expand.next('div')
	var  chevright = expand.find('span.chevright')
	var  chevdown = expand.find('span.chevdown')
	if (expandcontent.is(':visible')) {
		expandcontent.slideToggle(250);
		chevright.toggle();
		chevdown.toggle();
	} else {
		expandcontent.slideToggle(250);
		chevright.toggle();
		chevdown.toggle();
		chevdown[0].style.display = 'inline-block';
		}
	}