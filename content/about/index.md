---
title: "About"
description: "About Us page."
layout: "single"
showTableOfContents: true
showRelatedPosts: true
showBreadcrumbs: true
---
# About us
GCentral is a non profit organization providing a platform for G code packages and collaboration resources. GCentral is for programmers who need to find, share, or co-develop G reusable code or software engineering tools. GCentral is independent and driven by community experts.

# Our mission
To enable the LabVIEW community to make the best version of itself.<br>To improve our community's capability by removing barriers to collaboration.

# Special Thanks
*****Thanks to those who made GCentral possible.*****

GCentral is a non profit organization providing a platform for G code packages and collaboration resources. GCentral is for programmers who need to find, share, or co-develop G reusable code or software engineering tools. GCentral is independent and driven by community experts.

***Learn about GCentral from the CLA Summit 2019 - Austin presentation***
{{< youtube H4nFQeP3AYU >}}

# Board of Directors
- Quentin Alldredge
- Chris Cilino
- Danielle Jobe
- Elijah Kerry
- François Normandin

# Steering Committee
- Michael Aivaliotis
- Brian Hoover
- Jeff Kodosky
- Jon McBee
- Brian Powell
- Michal Radziwan
- Derek Trepanier
